"""This file is executed on every boot (including wake-boot from deepsleep)"""

# import esp
# esp.osdebug(None)
import gc
import time
import network
# import webrepl
from config import KNOWN_WIFI_AP

# Disable AP
WLAN = network.WLAN(network.AP_IF)
WLAN.active(False)

# Connect to one of the known wifi AP
WLAN = network.WLAN(network.STA_IF)
WLAN.active(True)
AP_LIST = WLAN.scan()

for ap in AP_LIST:
    if WLAN.isconnected():
        break

    ap_ssid = ap[0].decode("utf-8")
    if ap_ssid in KNOWN_WIFI_AP.keys():
        print("Known wifi network found : {}".format(ap_ssid))
        print("Try to connect...")
        WLAN.connect(ap_ssid, KNOWN_WIFI_AP[ap_ssid])

    DELAY = 0

    while not WLAN.isconnected():
        print("Waiting for wifi to connect...")
        time.sleep(1)
        DELAY += 1
        if DELAY > 10:
            print("Wifi time-out")
            break

# webrepl.start()

print("Boot.py : Done")
gc.collect()
