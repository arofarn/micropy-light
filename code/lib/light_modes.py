# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Modes for neopixels strip
Each function update the NeoPixel strip buffer (except update_neopixel() that
call them)
"""
__author__ = "arofarn"
__version__ = 0.1

import time
import urandom


def update_neopixel(mode, np_strp, col=(255, 255, 255), bri=50):
    """Update NeoPixel strip with one color
    :param np_strp : NeoPixel object
    :param mode : mode ;)
    :param col : color [R, G, B] (default: [255, 255, 255])
    :param bri : brightness 0-100 (default: 50)
    """
    if bri > 100:
        bri = 100
    elif bri < 0:
        bri = 0
    bri_coef = bri/100
    # Apply mode
    MODES_LST[mode](np_strp, col)
    # Apply brightness to the whole LEDs
    np_strp.buf = bytearray([int(x * bri_coef) for x in np_strp.buf])
    # Finally display color on LEDs
    np_strp.write()


def fill_usr(np_strp, col):
    """Update NeoPixel strip with one color
    :param np_strp : NeoPixel object
    :param col : color [R, G, B]
    """
    np_strp.fill(col)


def fill_white(np_strp, col=(255, 255, 255)):
    """Update NeoPixel strip with only white
    :param np_strp : NeoPixel object
    :param col : color [R, G, B] (ignored, default=(255, 255, 255))
    """
    col = (255, 255, 255)
    np_strp.fill(col)


def sparkles(np_strp, col):
    """Make Neopixel sparkle with user defined color!!!"""
    np_strp.fill((0, 0, 0))

    for _ in range(int(np_strp.n / 4)):
        pix = int(urandom.getrandbits(8) / 256 * (np_strp.n))
        np_strp[pix] = col


# def christmas(np_strp, col):
#     """Shine like christmas tree °<:oD
#     TODO !!!
#     """


MODES_LST = (fill_usr, fill_white, sparkles)
