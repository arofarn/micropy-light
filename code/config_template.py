"""Config file"""

known_wifi_ap = {"wifi_ssid": "wifi_wpa_key",
                 "wifi_ssid_2": "wifi_wpa_key_2",
                 }

CLIENT_ID = "desktop_light"
LOCATION = "desktop"

MQTT_HOST = "192.168.0.XXX"
MQTT_PORT = 1883
MQTT_USERNAME = "user"
MQTT_PASSWD = "blabla"
MQTT_TOPIC = "home/desk/desktop_light"

# NEOPIXEL strings
NB_PIX = 67                  # Nombre de pixels de la 1e bande de LED
NB_PIX2 = 67                 # Nombre de pixels de la 2e bande de LED
MAX_BRIGHT = 100             # Luminosité max (100 max.)
USR_COLOR = [255, 130, 20]   # Couleur de base (à luminosité max)
