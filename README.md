# micropy-light

Éclairage RGB connecté à base de Neopixel (LED RGB adressable type WS2812), de microcontrolleur ESP8266 sous micropython.

Liste de composants:
 - un microcontrôleur ESP8266 (par ex. Adafruit Feather HUZZAH)
 - un ruban ou un anneau de LED neopixel
 - un encoder rotatif (avec clic central) pour le contrôle direct
 - une puce SN74AHCT125N qui servira à adapter le niveau des signaux (3.3V côté microcontrôleur, 5V côté Neopixel)
 - une alimentation 5V suffisante pour le nombre de LED (compter 60mA/LED soit 300mW/LED)

Facile à adapter à un microcontrolleur ESP32 (quelques déclarations de broches).
